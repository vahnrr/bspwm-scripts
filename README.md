# bspwm-scripts

A collection of POSIX compliant shell scripts.

## Usage

### scratchpad

Works similarly to i3's scratchpad, except for the `--toggle` option.

#### --toggle

Given a X11 class name, the scratchpad will bring out or hide in the
corresponding node. This node however, won't be included in the scratchpad, so
`--cycle` won't show it ever.

It was a functionality I was missing from i3. It separates the temporary
scratchpad nodes (i.e. manually registered ones) from the few specific ones I
always want to keep in the backgound (feed reader and music player).

#### Remove a node when it's closed/killed

Using this scratchpad means that we need to inform it when a node is
closed/killed by bspwm. The current implementation **requires** to subscribe to
the `node_remove` event.

> Relevant *bspwmrc* sample:

``` sh
bspc subscribe node_remove | while read -r _ _ _ node_id; do
    scratchpad --remove "$node_id"
done
```

### sxhkd-watcher

Creates a *mode* indicator similar to the one possible polybar's i3 module. The
module (`hotkeys`) pulls the content of the *sxhkd-chain.txt* file everytime it
receives an ipc message from the *sxhkd-watcher* script. The script reads the
*sxhkd.fifo* file for events.

The modes needs to be explicitly defined in the *sxhkd-watcher* script. The
command `set_chain_label 'apps' 'm w' 'b c f o t v'` will print the mode as
`apps`, having the sub-modes `m` and ` w`, and with these available hotkeys: `b
c f o t v`.

![sxhkd-watcher](screenshots/sxhkd-watcher.png)

> Relevant *bspwmrc* sample:

``` sh
sxhkd_fifo="${XDG_RUNTIME_DIR:-/tmp}/sxhkd.fifo"
[ -p "$sxhkd_fifo" ] || mkfifo "$sxhkd_fifo"
sxhkd -s "$sxhkd_fifo" &

polybar <bar-name> &
sxhkd-watcher <bar-name> &
```

> Relevant *polybar* module:

``` conf
[module/hotkeys]
type = custom/ipc
hook-0 = printf ''
hook-1 = cat "${XDG_RUNTIME_DIR:-/tmp}/sxhkd-chain.txt"
initial = 1
format = <output>
```
